package com.app.banking.service.impl;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.app.banking.dto.AccountDTO;
import com.app.banking.dto.FundTransferDTO;
import com.app.banking.dto.UserDTO;
import com.app.banking.entity.Account;
import com.app.banking.entity.Transaction;
import com.app.banking.entity.User;
import com.app.banking.exception.InvalidAccountException;
import com.app.banking.exception.LowBalanceException;
import com.app.banking.repository.AccountRepository;
import com.app.banking.repository.TransactionRepository;
import com.app.banking.repository.UserRepository;
import com.app.banking.service.FoodBucketBankingServices;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class FoodBucketBankingServicesImpl implements FoodBucketBankingServices {

	SimpleDateFormat simpDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	UserRepository userRepository;

	@Autowired
	AccountRepository accRepository;

	@Autowired
	TransactionRepository txnRepository;

	@Override
	public String registerUser(UserDTO userDTO) {
		User user = objectMapper.convertValue(userDTO, User.class);
		userRepository.saveAndFlush(user);
		return "User registered successfully...";
	}

	@Override
	public Account createAccount(AccountDTO accDTO,User userData) {
		Optional<User> custOpt = userRepository.findById(accDTO.getUserId());
		if (custOpt.isPresent()) {
			User user = custOpt.get();
			Account account = new Account();
			account.setAccNumber(accountNumber());
			account.setBankName(accDTO.getBankName());
			account.setCardNumber(cardNo(16));
			account.setCvv(cardNo(3));
			LocalDate expiryDate = LocalDate.now().plusDays(365 * 10);
			account.setCardExpiryDate(expiryDate);
			account.setBalance(accDTO.getBalance());
			account.setContactNumber(accDTO.getContactNumber());
			account.setAccCreateDate(accDTO.getAccCreateDate());
			account.setStatus(accDTO.getStatus());
			account.setUser(user);
			accRepository.saveAndFlush(account);		
			
		return account;
		}
		return null;
	}
    
	private long accountNumber() {
		Random generator = new Random(System.currentTimeMillis());
		long acctNum = generator.nextInt(1000) + 9999;  
		return acctNum;
	}
	
	public static String cardNo(int n) {
		StringBuilder s = new StringBuilder("0123456789");
		StringBuilder cardNo = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (s.length() * Math.random());
			cardNo.append(s.charAt(index));
		}
		return cardNo.toString();
	}
	
	@Override
	public String transferFunds(FundTransferDTO ftDTO) throws InvalidAccountException, LowBalanceException {

		String message = "";

		Account fromAccount = null;
		if (ftDTO.getPaymentType().equals("card")) {
			fromAccount = accRepository.findByCardNumberAndCvvAndCardExpiryDate(ftDTO.getFromAccount(),
					ftDTO.getAccountCvv(), ftDTO.getCardExpiryDate());
		}
		else if (ftDTO.getPaymentType().equals("accountNumber")) {
			fromAccount = accRepository.findByAccNumber(ftDTO.getFromAccount());
		}
		else {
			fromAccount = accRepository.findByAccNumber(ftDTO.getFromAccount());
		}
		Account toAccount = accRepository.findByAccNumber(ftDTO.getToAccount());

		float txnAmount = ftDTO.getTxnAmount();

		if (fromAccount == null || toAccount == null) {
			throw new InvalidAccountException("Invalid Bank details provided...");
		}

		if (StringUtils.isNotEmpty(message)) {
			return message;
		} else {
			if (fromAccount != null) {
				float accBalance = fromAccount.getBalance();

				if (accBalance < ftDTO.getTxnAmount()) {
					throw new LowBalanceException("Low Balance...");
				} else {
					fromAccount.setBalance(fromAccount.getBalance() - ftDTO.getTxnAmount());
					accRepository.saveAndFlush(fromAccount);

					Transaction dtTxn = new Transaction();
					dtTxn.setAccount(fromAccount);
					dtTxn.setTxnType("Debit");
					dtTxn.setDate(java.sql.Timestamp.valueOf(simpDate.format(new Date())));
					dtTxn.setFromAccount(ftDTO.getFromAccount());
					dtTxn.setToAccount(ftDTO.getToAccount());
					dtTxn.setTxnAmount(txnAmount);
					txnRepository.saveAndFlush(dtTxn);
				}
			}

			if (toAccount != null) {
				toAccount.setBalance(toAccount.getBalance() + ftDTO.getTxnAmount());
				accRepository.saveAndFlush(toAccount);

				Transaction crTxn = new Transaction();
				crTxn.setAccount(toAccount);
				crTxn.setTxnType("Credit");
				crTxn.setDate(java.sql.Timestamp.valueOf(simpDate.format(new Date())));
				crTxn.setFromAccount(ftDTO.getFromAccount());
				crTxn.setToAccount(ftDTO.getToAccount());
				crTxn.setTxnAmount(txnAmount);
				txnRepository.saveAndFlush(crTxn);
			}

			message = "Transaction has been done successfully...";

		}
		return message;
	}

	public Account findAccountByaccountNumber(String accountNumber) {
		return accRepository.findByAccNumber(accountNumber);
	}

	public List<Account> findAccountBycontactNumber(String contactNumber) {
		return accRepository.findBycontactNumber(contactNumber);
	}

	public List<Transaction> getlatest5TransactionHistory(long userid) {
		PageRequest latest5Transactions = PageRequest.of(0, 5);
		List<Transaction> stmt = userRepository.findstmtById(userid, latest5Transactions);
		return stmt;
	}

	public List<Transaction> getMonthlybankstatement(String month) {
		return txnRepository.findRecordsByMonth(month);
	}

}
