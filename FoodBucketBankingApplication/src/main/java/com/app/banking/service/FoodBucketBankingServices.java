package com.app.banking.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.app.banking.dto.AccountDTO;
import com.app.banking.dto.FundTransferDTO;
import com.app.banking.dto.UserDTO;
import com.app.banking.entity.Account;
import com.app.banking.entity.Transaction;
import com.app.banking.entity.User;
import com.app.banking.exception.InvalidAccountException;
import com.app.banking.exception.LowBalanceException;

public interface FoodBucketBankingServices {
	
	public String registerUser(UserDTO userDTO);
	
	public Account createAccount(AccountDTO accDTO,User user);
	
	public String transferFunds(FundTransferDTO ftDTO) throws InvalidAccountException,LowBalanceException;
	
	public Account findAccountByaccountNumber(String accountNumber);
	
	public List<Account> findAccountBycontactNumber(String contactNumber);
		
	public List<Transaction> getlatest5TransactionHistory(long userid);
	
	public List<Transaction> getMonthlybankstatement(String month);
}
