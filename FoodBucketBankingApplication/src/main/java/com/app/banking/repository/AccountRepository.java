package com.app.banking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.banking.entity.Account;
import com.app.banking.entity.User;

@Repository
public interface AccountRepository extends JpaRepository<Account,Integer>{
	
	
	@Query("Select c from Account c where c.accNumber = :accNumber")
	Account findByAccNumber(@Param("accNumber") String accNumber);	

    Account findByCardNumberAndCvvAndCardExpiryDate(String cardNumber,String cvv,String cardExpiryDate);

	List<Account> findBycontactNumber(String contactNumber);	
}
