package com.app.banking.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.banking.entity.Transaction;
import com.app.banking.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	List<Transaction> findstmtById(long userid, Pageable latest5Transactions);
}
