package com.app.banking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.banking.entity.Account;
import com.app.banking.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	
	Optional<List<Transaction>> findByAccount(Account account);
		
	@Query("select t from Transaction t where t.date like %:month% ")
	List<Transaction> findRecordsByMonth(String month);
	

}
