package com.app.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.banking.dto.AccountDTO;
import com.app.banking.dto.FundTransferDTO;
import com.app.banking.dto.UserDTO;
import com.app.banking.entity.Account;
import com.app.banking.entity.Transaction;
import com.app.banking.entity.User;
import com.app.banking.exception.InvalidAccountException;
import com.app.banking.exception.LowBalanceException;
import com.app.banking.service.FoodBucketBankingServices;

@RestController
@RequestMapping("/banking")
public class FoodBucketBankingServicesController {
	
	@Autowired
	Environment environment;

	@Autowired
	FoodBucketBankingServices bankingServices;
	
	@GetMapping("/appPortInfo") 
	public String getPortInfo(){
		String port = environment.getProperty("local.server.port");		
		return port;
	}
	
	@PostMapping("/registerUser")
	public ResponseEntity<String> saveUser(@RequestBody UserDTO custDTO) {
		return new ResponseEntity<>(bankingServices.registerUser(custDTO), HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/createAccount")
	public Account createAccount(@RequestBody AccountDTO accDTO,User userData) {
		return bankingServices.createAccount(accDTO,userData);		
	}
	
	@PostMapping("/fundTransfer")
	public ResponseEntity<String> transferFunds(@RequestBody FundTransferDTO ftDTO)  throws InvalidAccountException,LowBalanceException {
		return new ResponseEntity<>(bankingServices.transferFunds(ftDTO), HttpStatus.OK);		
	}
	
	@GetMapping(value="/{month}")
    public ResponseEntity<List<Transaction>> getMonthlybankstatement(@PathVariable String month){
		List<Transaction> monthlybankstatement = bankingServices.getMonthlybankstatement(month);
		if(monthlybankstatement.isEmpty()) 
	    return new ResponseEntity(monthlybankstatement,HttpStatus.NOT_FOUND);
		else
		return new ResponseEntity(monthlybankstatement,HttpStatus.OK);
	}	
	
	@GetMapping(value="/{userId}")
	public List<Transaction> getlatest5TransactionHistory(@PathVariable("userid")Integer userId){
		return  bankingServices.getlatest5TransactionHistory(userId);
	}	
	
	@GetMapping("byContactNum/{contactNumber}")
	public List<Account> findAccountDetailsBycontactNumber(@PathVariable("contactNumber") String contactNumber) {
		return bankingServices.findAccountBycontactNumber(contactNumber);		
	}
	
	@GetMapping("byAccountNum/{accountNumber}")
	public Account findAccountDetailsByaccountNumber(@PathVariable("accountNumber") String accountNumber) {
		return bankingServices.findAccountByaccountNumber(accountNumber);		
	}
	
}
