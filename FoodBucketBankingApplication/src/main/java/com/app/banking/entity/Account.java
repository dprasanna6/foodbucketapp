package com.app.banking.entity;

import java.sql.Timestamp;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "acc_id")
	private Integer accId;

	@Column(name = "acc_number")
	private long accNumber;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "balance")
	private float balance;
	private String contactNumber;
	
	private String cardNumber;
	private String cvv;
	private LocalDate cardExpiryDate;
	private Timestamp accCreateDate;
	private String message;
    private String status;
	@ManyToOne
	@JoinColumn(name = "user_id")
	@JsonBackReference
	private User user;

}
