package com.app.banking.dto;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {
	private String accNumber;
	private String bankName;
	private float balance;
	private int userId;
	private String cardNumber;
	private String accountCvv;
	private String cardExpiryDate;
	private Timestamp accCreateDate;
	private String status;
	private String contactNumber;
}
