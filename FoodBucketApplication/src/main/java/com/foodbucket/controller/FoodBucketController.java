package com.foodbucket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodbucket.entity.Items;
import com.foodbucket.entity.OrderItem;
import com.foodbucket.entity.Transaction;
import com.foodbucket.exception.FoodItemNotFound;
import com.foodbucket.exception.UserNotFoundException;
import com.foodbucket.feignclient.FoodBucketBankingClient;
import com.foodbucket.model.FundTransferDTO;
import com.foodbucket.model.OrderDTO;
import com.foodbucket.model.UserRegisterDTO;
import com.foodbucket.service.FoodBucketInfoService;

@RestController
@RequestMapping("/foodService")
public class FoodBucketController {
	private static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	FoodBucketBankingClient bankingClient;	

	@Autowired
	FoodBucketInfoService foodBucketService;


	@GetMapping("/appPort")
	public String getAppPort() {
		return "Hiii";
	}


	@PostMapping("/user/registerUser")
	public ResponseEntity<String> registerUser(@RequestBody UserRegisterDTO registerDto) throws UserNotFoundException {
		return new ResponseEntity<>(foodBucketService.registerUser(registerDto), HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/fundTransfer")
	public ResponseEntity<String> transferFunds(@RequestBody FundTransferDTO ftDTO) {
		return bankingClient.transferFunds(ftDTO);
	}

	@GetMapping("/byVendorId/{vid}")
	public ResponseEntity<List<Items>> findItemsByVendorId(@PathVariable("vid") int vid) {
		return new ResponseEntity<>(foodBucketService.searchItemsByVendorId(vid), HttpStatus.OK);
	}


	@GetMapping("/byItemName/{itemName}")
	public ResponseEntity<List<Items>> findItemsByItemName(@PathVariable("itemName") String itemName) {
		return new ResponseEntity<>(foodBucketService.searchItemsByItemName(itemName), HttpStatus.OK);
	}

	@GetMapping("/getLatestFiveTransactions/{userid}")
	public ResponseEntity<List<OrderItem>> getlatestfiveOrders(@PathVariable("userid") int userid){
		return new ResponseEntity<>(foodBucketService.getlatestfiveOrders(userid), HttpStatus.OK);
	}
	
	@GetMapping("/{custId}/{txnMode}/statement")
	public List<Transaction> getMonthlybankstatement(@PathVariable String month){
		return bankingClient.getMonthlybankstatement(month);

	}

	@PostMapping("/user/orderFoor")
	public ResponseEntity<String> orderFood(@RequestBody OrderDTO ordersDTO) throws FoodItemNotFound, UserNotFoundException {
		String message = foodBucketService.orderFood(ordersDTO);		
			FundTransferDTO fndTx = objectMapper.convertValue(ordersDTO, FundTransferDTO.class);
			ResponseEntity<String> txnStatus = bankingClient.transferFunds(fndTx);
			if (txnStatus.equals("success")) {
				return new ResponseEntity<>("Placed Orderd succssfully.", HttpStatus.OK);
			}	

		return new ResponseEntity<>("Failed to create order", HttpStatus.OK);
	}

}

