package com.foodbucket.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "items")
public class Items {	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer itemId;		
    private String itemName;
    private String description;
    private double price;
    private String category;
    private int vid;
    private String date;
    
//    @ManyToOne
//	@JoinColumn(name = "vid")
//	@JsonBackReference
//	private Vendor vendor;
}
