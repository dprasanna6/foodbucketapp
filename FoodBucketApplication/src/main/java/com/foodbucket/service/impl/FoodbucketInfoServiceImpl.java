package com.foodbucket.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodbucket.entity.Account;
import com.foodbucket.entity.Items;
import com.foodbucket.entity.OrderItem;
import com.foodbucket.entity.User;
import com.foodbucket.exception.FoodItemNotFound;
import com.foodbucket.exception.UserNotFoundException;
import com.foodbucket.feignclient.FoodBucketBankingClient;
import com.foodbucket.model.ItemDetailsDTO;
import com.foodbucket.model.OrderDTO;
import com.foodbucket.model.PlacedOrderDTO;
import com.foodbucket.model.UserRegisterDTO;
import com.foodbucket.repository.ItemsRepository;
import com.foodbucket.repository.OrderRepository;
import com.foodbucket.repository.UserRepository;
import com.foodbucket.repository.VendorRepository;
import com.foodbucket.service.FoodBucketInfoService;


import antlr.StringUtils;

@Service
public class FoodbucketInfoServiceImpl implements FoodBucketInfoService{

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	ItemsRepository itemRepo;	
	
	@Autowired
	VendorRepository vendorRepo;
	
	@Autowired
	FoodBucketBankingClient bankingClient;
	
	@Autowired
	OrderRepository orderRepo;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	@Override
	public String registerUser(UserRegisterDTO registerDto) throws UserNotFoundException {
		
		// TODO Auto-generated method stub
		User userData=new User();	
		if (registerDto.getUserId() > 0) {
			userData.setUserId(registerDto.getUserId());
		}
		else
		{
			throw new UserNotFoundException("User not found");
		}
		if (!registerDto.equals(null)) 
		{
			userData.setFirstName(registerDto.getFirstName());
			userData.setLastName(registerDto.getLastName());
			userData.setEmail(registerDto.getEmail());
			userData.setContactNumber(registerDto.getContactNumber());
			userData.setAddress(registerDto.getAddress());		
		}
		userRepo.save(userData);
		return "User Registered successfully";
		}	
	
	
	public List<Items> searchItemsByVendorId(int vid) {		
		List<Items> foodItemsList = null;		
		foodItemsList = itemRepo.findByVid(vid);		
		return foodItemsList;		
	}
	
	public List<Items> searchItemsByItemName(String itemName) {
		List<Items> foodItemsList = null;		
		foodItemsList = itemRepo.findAllByItemName(itemName);		
		return foodItemsList;		
	}
	
	public List<OrderItem> getlatestfiveOrders(int userid) {
//		List<Order> listOftenOrders=  userRepo.findFirst10ByUserId(userid);
//		User user=new User();
//		listOftenOrders.stream().forEach(lst->user.setOrders(lst.getOrders()));
		
		Pageable latestFiveOrders=PageRequest.of(0, 5);
		List<User> listOfOrders=  userRepo.findByUserId(userid, latestFiveOrders);
		User user=new User();
		listOfOrders.stream().forEach(lst->user.setOrders(lst.getOrders()));
		List<OrderItem> orderdetails=user.getOrders();	
		orderdetails.stream().forEach(orderlst->System.out.println(""+orderlst.getOrderId()));
		return orderdetails;
	}
	
	
	public List<User> findUserByContactNumber(String contactNumber) {
		return userRepo.findByContactNumber(contactNumber);
	}
	
	
	public String orderFood(OrderDTO ordersDTO) throws FoodItemNotFound, UserNotFoundException {		
		Optional<User> userValue=null;
		if (ordersDTO.getUserId()>0 ){ 
			userValue = userRepo.findById(ordersDTO.getUserId());
	     	}
			if (userValue.isPresent()) {
				User user = userValue.get();
				OrderItem order = new OrderItem();
				order.setUser(user);
				order.setOrdereddate(dateFormat.format(new Date()));
				order.setAmount(ordersDTO.getAmount());
				order.setItemName(ordersDTO.getItems());
				order.setItemType(order);
				order = orderRepo.save(order);

				Items orderItem = null;
				Optional<Items> itemOpt = null;

				for (Integer itemId : ordersDTO.getItems()) {
					itemOpt = itemRepo.findById(itemId);
					if (itemOpt.isPresent()) {
						orderItem = new Items();
						orderItem.setItemId(itemId);						
						itemRepo.save(orderItem);

					} else {
						throw new FoodItemNotFound("foodItem is not available with ID: " + itemId.toString());

					}
				}
			}
			else {
				throw new UserNotFoundException("User Not Found");
			}
			return "order created Successfully";
	}
}
			
			

	
		
	





