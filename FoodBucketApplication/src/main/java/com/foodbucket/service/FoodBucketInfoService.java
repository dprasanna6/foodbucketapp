package com.foodbucket.service;

import java.util.List;

import com.foodbucket.entity.Items;
import com.foodbucket.entity.OrderItem;
import com.foodbucket.entity.User;
import com.foodbucket.exception.FoodItemNotFound;
import com.foodbucket.exception.UserNotFoundException;
import com.foodbucket.model.OrderDTO;
import com.foodbucket.model.UserRegisterDTO;

public interface FoodBucketInfoService {
	public String registerUser(UserRegisterDTO registerDto)throws UserNotFoundException;
	public List<Items> searchItemsByVendorId(int vid);
	public List<Items> searchItemsByItemName(String itemName);
	public List<OrderItem> getlatestfiveOrders(int userid);
	public List<User> findUserByContactNumber(String contactNumber);
	public String orderFood(OrderDTO ordersDTO) throws FoodItemNotFound, UserNotFoundException;
}
