package com.foodbucket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodbucket.entity.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor,String> {

}
