package com.foodbucket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodbucket.entity.OrderItem;

@Repository
public interface OrderRepository extends JpaRepository<OrderItem,Long> {

}
