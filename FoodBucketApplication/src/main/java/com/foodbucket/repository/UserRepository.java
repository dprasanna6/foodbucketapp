package com.foodbucket.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodbucket.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	 List<User> findByUserId(Integer userId, Pageable latestFiveOrders);
	 List<User> findFirst10ByUserId(Integer userId);
	 List<User> findByContactNumber(String contactNumber);
}
