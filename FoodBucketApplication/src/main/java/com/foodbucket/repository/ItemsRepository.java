package com.foodbucket.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodbucket.entity.Items;

@Repository
public interface ItemsRepository extends JpaRepository<Items,Integer>{		
	List<Items> findByVid(int vid);
	List<Items> findAllByItemName(String itemName);
}
