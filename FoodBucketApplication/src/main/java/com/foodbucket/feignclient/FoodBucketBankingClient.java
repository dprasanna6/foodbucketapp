package com.foodbucket.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.foodbucket.entity.Account;
import com.foodbucket.entity.Transaction;
import com.foodbucket.model.FundTransferDTO;


@FeignClient(name="http:/FOOD-BUCKET-BANKING-SERVICE/foodbucketbanking/banking")
public interface FoodBucketBankingClient {
	
	@GetMapping("/info")
	public String getPort();

	@PostMapping("/fundTransfer")
	public ResponseEntity<String> transferFunds(@RequestBody FundTransferDTO ftDTO);
	
	@GetMapping(value="/{month}")
    public List<Transaction> getMonthlybankstatement(@PathVariable String month);
	
	@GetMapping(value="/{userId}")
	public List<Transaction> getlatest5TransactionHistory(@PathVariable("userid")Integer userId);
	
	@GetMapping("byContactNum/{contactNumber}")
	public List<Account> findAccountDetailsBycontactNumber(@PathVariable("contactNumber") String contactNumber);	
	
	@GetMapping("byAccountNum/{accountNumber}")
	public Account findAccountDetailsByaccountNumber(@PathVariable("accountNumber") String accountNumber);
}
