package com.foodbucket.batchconfig;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.foodbucket.entity.Items;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableBatchProcessing
@EnableScheduling
@Slf4j
@PropertySource(value = { "classpath:application.properties" })
public class UploadItemsBatchConfig {

	@Autowired
	DataSource datasource;

	@Autowired
	EntityManagerFactory entityManager;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Value("classpath:vendorItems.csv")
	 private Resource vendorInputResource;

	@Bean
	public Job readUserCSVFileJob() {
		return jobBuilderFactory.get("readUserCSVFileJob").incrementer(new RunIdIncrementer()).start(step()).build();
	}

	@Bean
	public Step step() {
		return stepBuilderFactory.get("step")
				.<Items, Items>chunk(1)
				.reader(reader()).processor(processor())
				.writer(jpawriter())
				.build();
	}

	@Bean
	public ItemProcessor<Items, Items> processor() {
		return new DBLogProcessor();
	}

	@Bean
	public FlatFileItemReader<Items> reader() {
		FlatFileItemReader<Items> itemReader = new FlatFileItemReader<Items>();
		itemReader.setLineMapper(lineMapper());
		itemReader.setLinesToSkip(1);
		itemReader.setResource(vendorInputResource);
		return itemReader;
	}

	@Bean
	public LineMapper<Items> lineMapper() {
		DefaultLineMapper<Items> lineMapper = new DefaultLineMapper<Items>();
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setNames(new String[] { "itemId", "itemName", "description", "price","category","vid","date"});
		lineTokenizer.setIncludedFields(new int[] { 0, 1, 2, 3,4,5,6,7});
		BeanWrapperFieldSetMapper<Items> fieldSetMapper = new BeanWrapperFieldSetMapper<Items>();
		fieldSetMapper.setTargetType(Items.class);
		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		return lineMapper;
	}

//	@Bean
//	public JdbcBatchItemWriter<Items> writer() {
//		JdbcBatchItemWriter<Items> itemWriter = new JdbcBatchItemWriter<Items>();
//		itemWriter.setDataSource(datasource);
//		itemWriter.setSql("INSERT INTO items (itemId, itemName,description,price,category,vid)"
//				+ " VALUES (:item_id, :item_name ,:description, :price,:category,:vid)");
//		itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Items>());
//		return itemWriter;
//
//	}

	@Bean
	public JpaItemWriter<Items> jpawriter() {
		JpaItemWriter<Items> userItemWriter = new JpaItemWriter<Items>();
		userItemWriter.setEntityManagerFactory(entityManager);
		System.out.println("executed");
		return userItemWriter;
	}

}
