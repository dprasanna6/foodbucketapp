package com.foodbucket.batchconfig;

import org.springframework.batch.item.ItemProcessor;

import com.foodbucket.entity.Items;
import com.foodbucket.entity.Vendor;

public class DBLogProcessor implements ItemProcessor<Items, Items>

{
	public Items process(Items items) throws Exception {
		System.out.println("Inserting Vendor : " + items);
		return items;
	}	
	
}
