package com.foodbucket.model;

import java.time.LocalDate;
import java.util.List;

import com.foodbucket.entity.Items;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
	private int userId;
	private int itemId;
	private String cardNumber;
	private String cvv;
	private String cardExpiryDate;	
	private double amount;
	private String remarks;
	private String contactNumber;
    List<Integer> items;
}
