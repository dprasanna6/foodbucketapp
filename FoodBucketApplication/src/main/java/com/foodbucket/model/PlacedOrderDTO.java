package com.foodbucket.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlacedOrderDTO {
	private long orderId;
	private String message;
	private String itemName;
	private int quantity;
	private double price;
}
