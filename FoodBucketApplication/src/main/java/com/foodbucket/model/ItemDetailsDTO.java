package com.foodbucket.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemDetailsDTO {
	private String itemName;
	private float itemPrice;
	private String date;
	private Integer vendorId;
	private String vendorName;
}
