package com.foodbucket.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterDTO {
	private int userId;
	private String firstName;	
	private String lastName;	
	private String address;	
	private String email;	
	private String contactNumber;
}
