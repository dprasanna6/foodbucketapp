package com.foodbucket.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FundTransferDTO {
	private int userId;
	private String fromAccount;
	private String cardNumber;
	private String accountCvv;
	private String cardExpiryDate;
	private String toAccount;
	private float txnAmount;
	private String transactionMode;
	private String PaymentType;
	
}
